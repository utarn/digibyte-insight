#!/bin/bash


# Anyone who want to download existing digibyte-insight-database.tar.gz,
# you can request the link from my email [utharn.b at rmutsb dot ac dot th]
# Require yum install aria2 pigz for multithread processing
cd /root/.digibyte
aria2c -x 16 https://www.zarimpun.com/digibyte-insight-database.tar.gz
tar xf digibyte-insight-database.tar.gz -I pigz
