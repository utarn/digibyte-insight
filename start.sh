#!/bin/bash

source /root/.bashrc

if [ ! -z "$IMAGE" ] && [ ! -f "/root/.digibyte/blocks/blk00000.dat" ] ; then
    cd /root/
    echo "Downloading from $IMAGE"
#    wget --tries=0 -O image.tar.gz $IMAGE
    aria2c -c -x 16 $IMAGE -o image.tar.gz
    tar xvfz image.tar.gz
    rm -f /root/image.tar.gz
fi

digibyted
until digibyte-cli getblockchaininfo > /dev/null 2>&1
do
    echo Waiting for digibyte wallet loading. It will take several minutes.
    sleep 30
done
block=$(digibyte-cli getblockchaininfo 2> /dev/null | jq '.blocks')
while [ $block -eq 0 ]; do
   echo Waiting digibyte to start syncing for some time first.
   sleep 30
   block=$(digibyte-cli getblockchaininfo 2> /dev/null | jq '.blocks')
done

header=$(digibyte-cli getblockchaininfo 2> /dev/null | jq '.headers')
echo "Now need for wallet to complete syncing first."
while [ $block -lt $header ]; do
  percent=$(( $block * 100 / $header ));
  echo "Fetching $block blocks of $header headers. $percent %";
  sleep 5;
  block=$(digibyte-cli getblockchaininfo 2> /dev/null | jq '.blocks')
  header=$(digibyte-cli getblockchaininfo 2> /dev/null | jq '.headers')
done

echo "Optimized TX fee. Done."
digibyte-cli settxfee 0
insightdb=$(shopt -s nullglob dotglob; echo /root/.insight/*)
if (( ${#insightdb} )); then
   cd /root/insight-api
   forever start --spinSleepTime=1000 --minUptime=10 -o out.log  insight.js
   sleep 10
   tail -f out.log
   sleep infinity
else
   echo "No insight database is found. Start syncing first. It may take 10-12 hours."
   echo "source /root/.bashrc && /root/insight-api/util/sync.js -v --rpc"
   source /root/.bashrc && /root/insight-api/util/sync.js -v --rpc
fi
