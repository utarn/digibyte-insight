FROM utarn/devcentos
USER root
WORKDIR /root
ARG USERNAME=user
ARG PASSWORD=pass
COPY start.sh .
COPY restore.sh .
COPY digibyte.tar.gz .
#RUN curl -Llo /digibyte.tar.gz https://github.com/digibyte/digibyte/releases/download/v7.17.2/digibyte-7.17.2-x86_64-linux-gnu.tar.gz
RUN chmod +x /root/start.sh \
    && tar xvfz digibyte.tar.gz  \
    && cd digibyte-7.17.2 \
    && /usr/bin/cp -r * /usr \
    && cd .. \
    && /usr/bin/rm -rf digibyte-7.17.2 \
    && /usr/bin/rm digibyte.tar.gz \
    && mkdir -vp .digibyte 

VOLUME /root/.insight
VOLUME /root/.digibyte
EXPOSE 14022
EXPOSE 12024
EXPOSE 3000

RUN echo -e "export INSIGHT_NETWORK='livenet'\n\
    export BITCOIND_DATADIR=~/.digibyte/\n\
    export INSIGHT_FORCE_RPC_SYNC=1\n\
    export INSIGHT_PUBLIC_PATH=public\n\
    export BITCOIND_USER=${USERNAME}\n\
    export BITCOIND_PASS=${PASSWORD}\n" >> /root/.bashrc
RUN echo -e "server=1\n\
    maxconnections=300\n\
    rpcallowip=0.0.0.0/0.0.0.0\n\
    server=1\n\
    daemon=1\n\
    rpcuser=${USERNAME}\n\
    rpcpassword=${PASSWORD}\n\
    txindex=1\n" > /root/.digibyte/digibyte.conf

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash \
    && source /root/.bashrc \
    && nvm install v10 \
    && npm install -g forever \
    && git clone https://github.com/DigiByte-Core/insight/ \
    && git clone https://github.com/DigiByte-Core/insight-api \
    && cd insight-api \
    && git checkout a9ddbe4538c50b220286294e2851df4c8db7c1a0 \
    && npm install \
    && ln -s ../insight/public/

CMD /root/start.sh
